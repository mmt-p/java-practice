/* Converting a larger type to a smaller size type: 
	i.e. double -> float -> long -> int -> char -> short -> byte
it is done by placing the type in parentheses in front of the value.*/
public class NarrowingCast{
	public static void main(String[] args) {
		double myDouble = 9.78;
		int myInt = (int) myDouble;

		System.out.println(myDouble);
		System.out.println(myInt);
	}
}