/* converting a smaller type to a larger type size
	i.e. byte -> short -> char -> int -> long -> float -> double
Widening casting is done automatically when passing a smaller size type to a larger size type.*/
public class WideningCast{
	public static void main(String[] args) {
		int myInt = 9;
		double myDouble = myInt;
		
		System.out.println(myInt);
		System.out.println(myDouble);
	}
}